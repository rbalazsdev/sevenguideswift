//
//  SGSProgram.swift
//  SevenGuideSwift
//
//  Created by Razvan Balazs on 31/05/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//
// Program Model 

import Foundation

class SGSProgram {
    var name: String?
    var startTime: String?
    var endtTime: String?
    var channelName: String?
    var rating: String?

    init (json: [String:String]) {
        self.name = json["name"]
        self.startTime = json["start_time"]
        self.endtTime = json["end_time"]
        self.channelName = json["channel"]
        self.rating = json["rating"]
    }
}