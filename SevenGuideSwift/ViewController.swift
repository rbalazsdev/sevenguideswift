//
//  ViewController.swift
//  SevenGuideSwift
//
//  Created by Razvan Balazs on 31/05/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//
// Presents a list of programs.

import UIKit

class ViewController: UITableViewController {
    var programList = [SGSProgram]()
    var totalPrograms = 0
    let loading = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundView = loading;
        self.tableView.registerClass(UITableViewCell.classForCoder(), forCellReuseIdentifier: "ProgramCellId")

        loading.startAnimating()
        SGSProgramService.sharedInstance.retrieveProgramListFromPageIndex(0, completion: { (totalCount, results) -> Void in
            self.programList = results as! [SGSProgram]
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.loading.stopAnimating()
                self.tableView.reloadData()
            })
        })
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.programList.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "ProgramCellId")
    
            cell.textLabel?.text = programList[indexPath.row].name
        cell.detailTextLabel?.text = NSString(format: "%@ - %@", programList[indexPath.row].startTime!, programList[indexPath.row].endtTime!) as? String
        return cell
    }

}

