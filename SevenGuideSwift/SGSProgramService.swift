//
//  SGSProgramService.swift
//  SevenGuideSwift
//
//  Created by Razvan Balazs on 31/05/2015.
//  Copyright (c) 2015 Razvan Balazs. All rights reserved.
//
// Singleton for learning purposes. :)

import Foundation
let urlPath: String = "http://www.whatsbeef.net/wabz/guide.php?start="

class SGSProgramService  {
    static let sharedInstance = SGSProgramService()


    /// Completion block to retrieve the results from API.
    func retrieveProgramListFromPageIndex(pageIndex:Int, completion: (totalCount: Int, results:[AnyObject]) ->Void) ->Void {
        let totalString: String = NSString(format: "%@%d", urlPath, pageIndex) as String
        println("requestUrl = \(totalString)")
        let url: NSURL = NSURL(string: totalString)!
        var request: NSURLRequest = NSURLRequest(URL: url)
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue()) { (response: NSURLResponse!,data: NSData!,error: NSError!) -> Void in
            var error: NSError
            var resultsArray = [SGSProgram]()
            if var json: NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as? NSDictionary {
                if let programs = json["results"] as? [NSDictionary] {
                    for item in programs {
                        resultsArray.append(SGSProgram(json:item as! Dictionary))
                    }
                    completion(totalCount: json["count"] as! Int,results:resultsArray)
                }
            } else {
                // No data.
            }
            
        }
        
    
    
    }
}